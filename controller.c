//
// Created by alex on 10/15/18.
//

#include "controller.h"
#include <gtk/gtk.h>
#include <stdlib.h>

static void button_clicked(GtkWidget *widget, gpointer data) {
    double r1, r2, r3;
    char *end;
    gboolean ok = TRUE;

    RotCylinderController *controller = data;

    r1 = strtod(gtk_entry_get_text(GTK_ENTRY(controller->view->r1_entry)), &end);
    ok = ok && (*end == '\0' && r1 > 0);

    r2 = strtod(gtk_entry_get_text(GTK_ENTRY(controller->view->r2_entry)), &end);
    ok = ok && (*end == '\0' && r2 > 0);

    r3 = strtod(gtk_entry_get_text(GTK_ENTRY(controller->view->r3_entry)), &end);
    ok = ok && (*end == '\0' && r3 > 0);

    if (ok) {
        rot_cylinder_model_update(controller->model, r1, r2, r3);
        g_print("All good\n");
    } else {
        // todo: handle error
        g_print("Malformed input\n");
    }
}

RotCylinderController *rot_cylinder_controller_new(RotCylinderModel *model, RotCylinderView *view) {
    RotCylinderController *controller = malloc(sizeof(*controller));
    controller->model = model;
    controller->view = view;

    g_signal_connect(view->button, "clicked", G_CALLBACK(button_clicked), controller);

    return controller;
}

void rot_cylinder_controller_delete(RotCylinderController *controller) {
    free(controller);
}