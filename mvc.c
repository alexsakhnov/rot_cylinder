//
// Created by alex on 10/16/18.
//

#include "mvc.h"
#include <gtk/gtk.h>
#include <stdlib.h>

static void
activate(RotCylinderMVC *mvc) {

    mvc->model = rot_cylinder_model_new(1000);
    mvc->view = rot_cylinder_view_new(mvc->app, mvc->model);
    mvc->controller = rot_cylinder_controller_new(mvc->model, mvc->view);

    gtk_widget_show_all(mvc->view->window);
}

RotCylinderMVC *
rot_cylinder_mvc_new() {
    RotCylinderMVC *mvc = malloc(sizeof(*mvc));

    // init app
    mvc->app = gtk_application_new("com.gitlab.alexsakhnov.rot_cylinder", G_APPLICATION_FLAGS_NONE);
    g_signal_connect_swapped(mvc->app, "activate", G_CALLBACK(activate), mvc);

    return mvc;
}

int rot_cylinder_mvc_run(RotCylinderMVC *mvc, int argc, char **argv) {
    return g_application_run(G_APPLICATION (mvc->app), argc, argv);
}

void rot_cylinder_mvc_delete(RotCylinderMVC *mvc) {

    rot_cylinder_model_delete(mvc->model);
    rot_cylinder_view_delete(mvc->view);
    rot_cylinder_controller_delete(mvc->controller);
    g_object_unref(mvc->app);
    free(mvc);
}