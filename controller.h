//
// Created by alex on 10/15/18.
//

#ifndef ROT_CYLINDER_CONTROLLER_H
#define ROT_CYLINDER_CONTROLLER_H

#include "model.h"
#include "view.h"

typedef struct {
    RotCylinderView *view;
    RotCylinderModel *model;
} RotCylinderController;

RotCylinderController *rot_cylinder_controller_new(RotCylinderModel *, RotCylinderView *);
void rot_cylinder_controller_delete(RotCylinderController *controller);

#endif //ROT_CYLINDER_CONTROLLER_H
