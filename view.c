//
// Created by alex on 10/12/18.
//

#include <gtk/gtk.h>
#include "view.h"
#include "model.h"
#include <stdlib.h>

/**
 * Возвращает модель данных, подогнанную под размер области рисования width x height.
 * Переменные x0, y0 содержат смещение начала координат.
 */
static RotCylinderModel *scale_model(RotCylinderModel *model, double width, double height, double *x0, double *y0) {
    // Опредение верхнего левого и нижнего правого угла видимой области
    double x1, x2, y1, y2;
    x1 = MIN(-model->r1, -model->r2);
    x2 = MAX(model->r1, model->r2);
    y1 = -model->r1;
    y2 = model->r1 + model->r2 * 2;

    for (int i = 0; i < model->path_length; ++i) {
        x1 = MIN(x1, model->path[i].x);
        x2 = MAX(x2, model->path[i].x);
        y1 = MIN(y1, model->path[i].y);
        y2 = MAX(y2, model->path[i].y);
    }

    // определяем коэффициент сжатия исходного графика
    double scale = MIN(width / (x2 - x1), height / (y2 - y1));

    // определяем смещение начала координат, чтобы график заполнил всю область для рисования
    double xc = (x2 + x1) / 2.0, yc = (y2 + y1) / 2.0;
    *x0 = width / 2.0 - xc * scale;
    *y0 = height / 2.0 + yc * scale;

    // сжимаем исходную модель и получаем сжатую модель
    RotCylinderModel *scaled_model = rot_cylinder_model_new(model->path_length);
    scaled_model->r1 = model->r1 * scale;
    scaled_model->r2 = model->r2 * scale;
    scaled_model->r3 = model->r3 * scale;

    for (int i = 0; i < model->path_length; ++i) {
        scaled_model->path[i].x = model->path[i].x * scale;
        scaled_model->path[i].y = model->path[i].y * scale;
    }

    return scaled_model;
}

static inline void set_color(GdkRGBA *color, double red, double green, double blue, double alpha) {
    color->red = red;
    color->green = green;
    color->blue = blue;
    color->alpha = alpha;
}

/**
 * Рисует содержимое области для рисования
 */
static gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer model) {
    double x0, y0;
    gint width, height;
    GdkRGBA color;
    GtkStyleContext *context;
    RotCylinderModel *scaled_model;

    if (!rot_cylinder_model_is_valid(model)) {
        return FALSE;
    }

    width = gtk_widget_get_allocated_width(widget);
    height = gtk_widget_get_allocated_height(widget);
    scaled_model = scale_model(model, width, height, &x0, &y0);

    context = gtk_widget_get_style_context(widget);

    gtk_render_background(context, cr, 0, 0, width, height);

    set_color(&color, 0.8, 0.8, 1, 1);
    gdk_cairo_set_source_rgba(cr, &color);
    cairo_paint(cr);

    // рисуем первый цилиндр
    cairo_arc(cr, x0, y0, scaled_model->r1, 0, 2 * G_PI);
    gtk_style_context_get_color(context, gtk_style_context_get_state(context), &color);
    set_color(&color, 0.5, 0.5, 1, 1);
    gdk_cairo_set_source_rgba(cr, &color);
    cairo_fill(cr);

    // рисуем второй цилиндр
    cairo_arc(cr, x0, y0 - (scaled_model->r2 + scaled_model->r1), scaled_model->r2, 0, 2 * G_PI);
    gtk_style_context_get_color(context, gtk_style_context_get_state(context), &color);
    set_color(&color, 0.3, 0.3, 1, 1);
    gdk_cairo_set_source_rgba(cr, &color);
    cairo_fill(cr);

    // рисуем траекторию
    cairo_new_path(cr);
    for (int i = 0; i < scaled_model->path_length; ++i) {
        cairo_line_to(cr, scaled_model->path[i].x + x0, -scaled_model->path[i].y + y0);
    }
    set_color(&color, 1, 1, 1, 1);
    gdk_cairo_set_source_rgba(cr, &color);
    cairo_stroke(cr);

    // очищаем память
    rot_cylinder_model_delete(scaled_model);

    return FALSE;
}

static void on_model_update(void *data) {
    gtk_widget_queue_draw(GTK_WIDGET(data));
}

/**
 * Создаёт GUI
 *
 * @param app
 * @return
 */
RotCylinderView *rot_cylinder_view_new(GtkApplication *app, RotCylinderModel *model) {
    RotCylinderView *view = malloc(sizeof(*view));

    GtkWidget *grid;
    GtkWidget *button_box;
    GtkWidget *draw_area;

    // init window
    view->window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW (view->window), "Вращение точки вокруг цилиндра");
    gtk_window_set_default_size(GTK_WINDOW (view->window), 500, 300);

    // create main grid
    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(view->window), grid);

    // add draw area
    draw_area = gtk_drawing_area_new();
    gtk_widget_set_hexpand(draw_area, TRUE);
    gtk_widget_set_vexpand(draw_area, TRUE);
    gtk_widget_set_size_request(draw_area, 100, 100);
    gtk_grid_attach(GTK_GRID(grid), draw_area, 0, 0, 1, 1);

    // add form grid
    GtkWidget *grid_form;
    grid_form = gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), grid_form, 1, 0, 1, 1);

    // init button in the box
    button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    view->button = gtk_button_new_with_label("Нарисовать");
    gtk_container_add(GTK_CONTAINER (button_box), view->button);

    view->r1_entry = gtk_entry_new();
    view->r2_entry = gtk_entry_new();
    view->r3_entry = gtk_entry_new();

    GtkWidget *widgets[] = {
            gtk_label_new("R1"),
            view->r1_entry,
            gtk_label_new("R2"),
            view->r2_entry,
            gtk_label_new("R3"),
            view->r3_entry,
            button_box
    };
    int column_size = sizeof(widgets) / sizeof(widgets[0]);
    for (int i = 0; i < column_size; ++i) {
        gtk_grid_attach(GTK_GRID(grid_form), widgets[i], 0, i, 1, 1);
    }

    gtk_widget_set_margin_bottom(view->r1_entry, 10);
    gtk_widget_set_margin_bottom(view->r2_entry, 10);
    gtk_widget_set_margin_bottom(view->r3_entry, 10);
    gtk_widget_set_margin_start(grid_form, 10);
    gtk_widget_set_margin_end(grid_form, 10);


    g_signal_connect(G_OBJECT(draw_area), "draw", G_CALLBACK(draw_callback), model);

    // register model observer
    rot_cylinder_model_register_observer(model, on_model_update, draw_area);

    return view;
}

void rot_cylinder_view_delete(RotCylinderView *view) {
    free(view);
}
