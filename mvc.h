//
// Created by alex on 10/16/18.
//

#ifndef ROT_CYLINDER_MVC_H
#define ROT_CYLINDER_MVC_H

#include "model.h"
#include "view.h"
#include "controller.h"
#include <gtk/gtk.h>

typedef struct {
    RotCylinderModel *model;
    RotCylinderView *view;
    RotCylinderController *controller;
    GtkApplication *app;
} RotCylinderMVC;

RotCylinderMVC *rot_cylinder_mvc_new();

int rot_cylinder_mvc_run(RotCylinderMVC *mvc, int argc, char **argv);

void rot_cylinder_mvc_delete(RotCylinderMVC *);

#endif //ROT_CYLINDER_MVC_H
