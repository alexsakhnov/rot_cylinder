//
// Created by alex on 10/12/18.
//

#ifndef ROT_CYLINDER_VIEW_H
#define ROT_CYLINDER_VIEW_H

#include <gtk/gtk.h>
#include "model.h"

typedef struct {
    GtkWidget *window;
    GtkWidget *r1_entry;
    GtkWidget *r2_entry;
    GtkWidget *r3_entry;
    GtkWidget *button;
} RotCylinderView;

RotCylinderView *rot_cylinder_view_new(GtkApplication *, RotCylinderModel *);

void rot_cylinder_view_delete(RotCylinderView *view);

#endif //ROT_CYLINDER_VIEW_H
