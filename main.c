#include <gtk/gtk.h>
#include "view.h"
#include "controller.h"
#include "mvc.h"


int
main(int argc, char **argv) {
    int status;
    RotCylinderMVC *mvc;

    mvc = rot_cylinder_mvc_new();
    status = rot_cylinder_mvc_run(mvc, argc, argv);

    // frees everything
    rot_cylinder_mvc_delete(mvc);

    return status;
}