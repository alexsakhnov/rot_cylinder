//
// Created by alex on 10/14/18.
//

#ifndef ROT_CYLINDER_MODEL_H
#define ROT_CYLINDER_MODEL_H

#include <stdbool.h>

struct _RotCylinderPoint {
    double x;
    double y;
};

typedef void (*RotCylinderModelObserver)(void *);

struct _RotCylinderModel {
    double r1;
    double r2;
    double r3;
    struct _RotCylinderPoint *path;
    unsigned int path_length;

    void *observer_data;
    RotCylinderModelObserver observer;
};

typedef struct _RotCylinderModel RotCylinderModel;

RotCylinderModel *rot_cylinder_model_new(unsigned int path_length);

void rot_cylinder_model_update(RotCylinderModel *model, double r1, double r2, double r3);

void rot_cylinder_model_register_observer(RotCylinderModel *model, RotCylinderModelObserver observer,
                                          void *observer_data);

bool rot_cylinder_model_is_valid(RotCylinderModel *);

void rot_cylinder_model_delete(RotCylinderModel *);

#endif //ROT_CYLINDER_MODEL_H
