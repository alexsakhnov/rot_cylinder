//
// Created by alex on 10/14/18.
//

#include <stdlib.h>
#include "model.h"
#include <math.h>

RotCylinderModel *rot_cylinder_model_new(unsigned int path_length) {
    RotCylinderModel *model = malloc(sizeof(*model));
    model->path_length = path_length;
    model->path = malloc(sizeof(*model->path) * model->path_length);
    model->observer = NULL;
    model->r1 = 0.0;
    model->r2 = 0.0;
    model->r3 = 0.0;

    return model;
}

void rot_cylinder_model_update(RotCylinderModel *model, double r1, double r2, double r3) {
    model->r1 = r1;
    model->r2 = r2;
    model->r3 = r3;

    double dt = 2.0 * M_PI / (model->path_length - 1), t = 0.0;
    double w1 = 1.0;
    double w2 = (1.0 + model->r1 / model->r2) * w1;
    for (int i = 0; i < model->path_length; ++i) {
        model->path[i].x = (model->r1 + model->r2) * sin(w1 * t) + model->r3 * sin(w2 * t);
        model->path[i].y = (model->r1 + model->r2) * cos(w1 * t) + model->r3 * cos(w2 * t);
        t += dt;
    }

    // notify observer
    if (model->observer != NULL) {
        model->observer(model->observer_data);
    }
}

void rot_cylinder_model_register_observer(RotCylinderModel *model, RotCylinderModelObserver observer,
                                          void *observer_data) {
    model->observer = observer;
    model->observer_data = observer_data;
}

void rot_cylinder_model_delete(RotCylinderModel *model) {
    free(model->path);
    free(model);
}

bool rot_cylinder_model_is_valid(RotCylinderModel *model) {
    return model->r1 > 0 && model->r2 > 0 && model->r3 > 0;
}
